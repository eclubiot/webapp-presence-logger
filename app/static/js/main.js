var refresh_time = 5;

function select_refresh_time() {
    var check_button = $('[name|="time-select"]:checked');
    //console.log(check_button.val());

    $('[id|="time-select-label"]').removeClass('btn-primary').addClass('btn-secondary');
    check_button.parent().removeClass('btn-secondary').addClass('btn-primary');

    update_refresh_time();
}

function update_refresh_time() {
    var check_button = $('[name|="time-select"]:checked');
    refresh_time = check_button.val();
    document.cookie = "refresh_time=" + refresh_time;
}

function update_tables() {
    setTimeout(arguments.callee, refresh_time * 1000);

    $.getJSON(
        $SCRIPT_ROOT + "/presence-logger/api/v1.0/present-users",
        function (data) {
            // Update the value in your table here
            $("[id|='presence-table-row']").remove();
            $.each(data, function (id, user) {
                $('#presence-table > tbody:last-child').append(
                    '<tr id="presence-table-row">' +
                    '<th scope="row"> <i class="material-icons" style="color:#75ff33">face</i> </th>' +
                    '<td>' + user.fullname + '</td>' +
                    '<td>' + user.timestamp + '</td>' +
                    '<td>' + user.place + '</td>' +
                    '</tr>'
                )
                ;
            });
        }
    );

    $.getJSON(
        $SCRIPT_ROOT + "/presence-logger/api/v1.0/users",
        function (data) {
            $("[id|='users-table-row']").remove();
            $.each(data, function (id, user) {
                $('#users-table > tbody:last-child').append(
                    '<tr id="users-table-row">' +
                    '<th>' + user.fullname + '</th>' +
                    '<td>' + user.uuid + '</td>' +
                    '</tr>');
            });
        }
    );

    $.getJSON(
        $SCRIPT_ROOT + "/presence-logger/api/v1.0/places",
        function (data) {
            $("[id|='places-table-row']").remove();
            $.each(data, function (id, place) {
                $('#places-table > tbody:last-child').append(
                    '<tr id="places-table-row">' +
                    '<th>' + place.name + '</th>' +
                    '<td>' + place.bssid + '</td>' +
                    '</tr>');
            });
        }
    );
}

function checkTime(i) {
    return (i < 10) ? "0" + i : i;
}

function start_time() {
    var today = new Date(),
        y = checkTime(today.getFullYear()),
        M = checkTime(today.getMonth()),
        d = checkTime(today.getDay()),
        h = checkTime(today.getHours()),
        m = checkTime(today.getMinutes()),
        s = checkTime(today.getSeconds());
    document.getElementById('clock').innerHTML = y + "-" + M + "-" + d + " " + h + ":" + m + ":" + s;
    setTimeout(arguments.callee, refresh_time * 1000);
}




