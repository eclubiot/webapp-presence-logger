import logging
import pendulum

from flask import current_app as app
from flask_restful import Resource, reqparse, abort
from flask_httpauth import HTTPBasicAuth

from datetime import datetime
from hashlib import sha512

from ..models import User, Log, Place
from .gets import get_present_users
from .. import db
from . import api, api_bp

auth = HTTPBasicAuth()


@auth.get_password
def get_password(username):
    """Function from HTTPBasicAuth provides user's password.

    Password is based on SECRET_KEY and used sha512 to hash it.

    Args:
        username (str): username of logging user

    Returns:
        str: password for got username

    """
    # print(sha512(app.config['SECRET_KEY'].encode('utf-8')).hexdigest())
    if username == 'root':
        return sha512(app.config['SECRET_KEY'].encode('utf-8')).hexdigest()

    return None


class PresentUsersAPI(Resource):
    """Unsecured REST API for logs of currently present users in club area."""

    def get(self, minutes_pass=10):
        """GET request."""
        logs = get_present_users(minutes_pass)
        return logs


class ProtectedPresentUsersAPI(Resource):
    """Secure REST API for logs of currently present users in club area.

    Is protected by HTTPBasicAuth. In request header needs to be user:password."
    """
    decorators = [auth.login_required]

    def __init__(self):
        """JSON format is parsed by reqparse.

        Fields:
            fullname (str, required): fullname of user
            uuid (str, required): uuid of user application
            bssid (str, required): bssid of router
        """
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('fullname', type=str, required=True, help='No fullname field provided',
                                   location='json')
        self.reqparse.add_argument('uuid', type=str, required=True, help='No UUID field provided', location='json')
        self.reqparse.add_argument('bssid', type=str, required=True, default='Unknown', location='json')

        super(ProtectedPresentUsersAPI, self).__init__()

    def post(self):
        """POST request.

        Check whether user with given uuid is already in DB. If no, user is created, otherwise is name update.
        Check whether place with given bssid is already in DB. If no, new place is created with 'Unknown' name.
        Timestamp is added in UTC format.
        """
        args = self.reqparse.parse_args()
        logging.info(f'POST request:\n {args}')

        # Query users
        user = User.query.filter_by(uuid=args['uuid']).first()
        # If user doesn't exist
        if user is None:
            user = User(fullname=args['fullname'], uuid=args['uuid'])
            db.session.add(user)
        # If user have different name
        elif user.fullname != args['fullname']:
            user.fullname = args['fullname']

        # Query place
        place = Place.query.filter_by(bssid=args['bssid']).first()
        if place is None:
            place = Place(name='Unknown', bssid=args['bssid'])
            db.session.add(place)

        # Add timestamp
        timestamp = pendulum.utcnow()
        log = Log(timestamp=timestamp, user=user, place=place)

        db.session.add(log)
        db.session.commit()

        return log.serialize


class UsersAPI(Resource):
    """Unsecured REST API for getting all users of club."""

    def get(self):
        """GET request."""
        users = User.query.all()

        return [user.serialize for user in users]


class ProtectedUsersAPI(Resource):
    """Secure REST API for updating club users.

    Is protected by HTTPBasicAuth. In request header needs to be user:password."
    """
    decorators = [auth.login_required]

    def __init__(self):
        """JSON format is parsed by reqparse.

        Fields:
            fullname (str): fullname of user
            bssid (str, required): bssid of router
        """

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('fullname', type=str, default='Unknown', location='json')
        self.reqparse.add_argument('uuid', type=str, required=True, help='No uuid field provided',
                                   location='json')

    def put(self):
        """PUT request.

        Check whether user with given uuid is already in DB. If yes user fullname is updated.
        """
        args = self.reqparse.parse_args()
        user = User.query.filter_by(uuid=args['uuid']).first()

        if user is None:
            abort(404, message=f"User with uuid={args['uuid']} doesn't exist.")

        user.fullname = args.get('fullname')

        db.session.commit()

        return user.serialize

    def post(self):
        """POST request.

        Check whether user with given uuid is already in DB. If no user is created.
        """
        args = self.reqparse.parse_args()
        user = User.query.filter_by(uuid=args['uuid']).first()

        if user is not None:
            abort(409, message=f"User with uuid={args['uuid']} already exists.")

        user = User(fullname=args.get('fullname'), uuid=args['uuid'])

        db.session.add(user)
        db.session.commit()

        return user.serialize

    def delete(self):
        """DEL request.

        Check whether user with given uuid is already in DB. If yes user is deleted.
        """
        args = self.reqparse.parse_args()
        user = User.query.filter_by(uuid=args['uuid']).first()

        if user is None:
            abort(404, message=f"User with uuid={args['uuid']} doesn't exist.")

        db.session.delete(user)
        db.session.commit()

        return user.serialize


class PlacesAPI(Resource):
    """Unsecured REST API for getting all places of club."""

    def get(self):
        """GET request."""
        places = Place.query.all()

        return [place.serialize for place in places]


class ProtectedPlacesAPI(Resource):
    """Secure REST API for updating club places.

    Is protected by HTTPBasicAuth. In request header needs to be user:password."
    """
    decorators = [auth.login_required]

    def __init__(self):
        """JSON format is parsed by reqparse.

        Fields:
            name (str, required): name of place
            bssid (str, required): bssid of router
        """

        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('name', type=str, default='Unknown', location='json')
        self.reqparse.add_argument('bssid', type=str, required=True, help='No bssid field provided',
                                   location='json')

    def put(self):
        """PUT request.

        Check whether place with given bssid is already in DB. If yes place name is updated.
        """
        args = self.reqparse.parse_args()
        place = Place.query.filter_by(bssid=args['bssid']).first()

        if place is None:
            abort(404, message=f"Place with bssid={args['bssid']} doesn't exist.")

        place.name = args.get('name')

        db.session.commit()

        return place.serialize

    def post(self):
        """POST request.

        Check whether place with given bssid is already in DB. If no place is created.
        """
        args = self.reqparse.parse_args()
        place = Place.query.filter_by(bssid=args['bssid']).first()

        if place is not None:
            abort(409, message=f"Place with bssid={args['bssid']} already exists.")

        place = Place(name=args.get('name'), bssid=args['bssid'])

        db.session.add(place)
        db.session.commit()

        return place.serialize

    def delete(self):
        """DEL request.

        Check whether place with given bssid is already in DB. If yes place is deleted.
        """
        args = self.reqparse.parse_args()
        place = Place.query.filter_by(bssid=args['bssid']).first()

        if place is None:
            abort(404, message=f"Place with bssid={args['bssid']} doesn't exist.")

        db.session.delete(place)
        db.session.commit()

        return place.serialize


api.add_resource(PresentUsersAPI, '/present-users', endpoint='present-users')
api.add_resource(ProtectedPresentUsersAPI, '/present-users', endpoint='protected-present-users')
api.add_resource(PresentUsersAPI, '/present-users/<int:minutes_pass>', endpoint='present-users-time')

api.add_resource(UsersAPI, '/users', endpoint='users')
api.add_resource(ProtectedUsersAPI, '/users', endpoint='protected-users')

api.add_resource(PlacesAPI, '/places', endpoint='places')
api.add_resource(ProtectedPlacesAPI, '/places', endpoint='protected-places')
