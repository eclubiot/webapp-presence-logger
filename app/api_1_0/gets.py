from datetime import datetime, timedelta
from ..models import Log


def list_of_seq_unique_by_key(seq, key):
    """Support function for create list with unique value in some key.

    Args:
         seq (:obj:`seq`): original sequence
         key (str): key which should be unique

    Returns:
        list: list with unique key
    """
    seen = set()
    return [x for x in seq if x[key] not in seen and not seen.add(x[key])]


def get_present_users(minutes_pass):
    """Support function for getting present user.

    Args:
        minutes_pass (int, optional): Set the threshold how old logs are not taken in notice. Default is 10 minutes.

    Returns:
        list: List of unique dictionaries in format

            [{
                "timestamp": "2017-07-12T08:40:09.764878+02:00",
                "fullname": "Jan Novak",
                "2017-07-12T08:40:09.764878+02:00"
            },]
    """
    timestamp = datetime.utcnow() - timedelta(minutes=minutes_pass)
    logs = Log.query.filter(Log.timestamp > timestamp).order_by(Log.timestamp.desc())

    logs = list(log.serialize for log in logs)
    logs = list_of_seq_unique_by_key(logs, 'fullname')

    return logs
