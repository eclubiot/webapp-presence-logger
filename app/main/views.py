from flask import render_template, request

from ..api_1_0.gets import get_present_users
from ..models import User, Log, Place
from . import main


@main.route('/')
@main.route('/index')
def index():
    """Function for index route.

    Description of project on Home page.
    """
    return render_template('index.html', title="Home")


@main.route('/presence-logger')
def presence_logger():
    """Function for presence-logger route.

    Dashboard with main information about present users is area
    """
    # Dictionary with all data
    data = {}

    # Check if request has cookie for refresh_time
    refresh_time = request.cookies.get('refresh_time')
    try:
        refresh_time = int(refresh_time)
    except (ValueError, TypeError):
        refresh_time = 5
    data['refresh_time'] = refresh_time

    # Refresh time menu
    times = [1, 5, 10, 15]
    data['times'] = times

    # Users data from DB
    users = User.query.all()
    data['users'] = users

    # Places data from DB
    places = Place.query.all()
    data['places'] = places

    # Logs data from DB
    # logs = Log.query.order_by(Log.timestamp.desc())
    # data['logs'] = logs

    # Present users in club area
    present_users = get_present_users(10)
    data['present_users'] = present_users

    return render_template('presence-logger.html',
                           title="Presence logger",
                           data=data)
