from app import db

import pendulum


class User(db.Model):
    """Class represent single member of the club.

    Attributes:
        id (id): primary key for table
        uuid (str): unique key string from application
        fullname (str): full name of user
        log (relation): relation with Log class
    """
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(64), nullable=False, index=True)
    fullname = db.Column(db.String(64), nullable=False, index=True)
    log = db.relationship('Log', backref='user', lazy='dynamic', cascade="save-update, merge, delete, delete-orphan")

    def __str__(self):
        return f'<User {self.fullname}>'

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'fullname': self.fullname,
            'uuid': self.uuid,
        }


class Place(db.Model):
    """Class represent single place.

    Attributes:
        id (id): primary key for table
        bssid (str): BSSID of AC in area
        name (str, optional): Name of the area
        log (relation): relation with Log class
    """
    id = db.Column(db.Integer, primary_key=True)
    bssid = db.Column(db.String(64), nullable=False, index=True)
    name = db.Column(db.String(64), index=True)
    log = db.relationship('Log', backref='place', lazy='dynamic', cascade="save-update, merge, delete, delete-orphan")

    def __str__(self):
        return f'<Place {self.name}>'

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'name': self.name,
            'bssid': self.bssid,
        }


class Log(db.Model):
    """Class represent log that user is present in area

    Attributes:
        timestamp (DateTime):  Timestamp of log
        user_id (int): ForeignKey from User class
        place_id (int): ForeignKey from Place class
    """
    timestamp = db.Column(db.DateTime, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    place_id = db.Column(db.Integer, db.ForeignKey('place.id'))

    def __str__(self):
        return f'<Log {self.user.fullname}>'

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        utc_tz = pendulum.timezone('utc')
        prague_tz = pendulum.timezone('Europe/Prague')
        t = utc_tz.convert(self.timestamp)
        t = prague_tz.convert(t)
        return {
            'timestamp': t.isoformat(),
            'fullname': self.user.fullname,
            'place': self.place.name,
        }
