# Presence-logger
Presence logger is Python web application written in Flask. It's a backend for android application with REST API.

## Installation
Install requirements by running command:  
`$ pip install -r requirements.txt`

Export environment variables:  
```
$ export FLASK_APP='flasky.py'
$ export FLASK_CONFIG='heroku'
$ export SECRET_KEY='secret-key'
$ export DATABASE_URL='...' # If is not declare default is SQLite in local directory
```

Initialize and run DB:  
```
$ flask db init
$ flask db migrate
$ flask db upgrade
```

Run it:  
`$ flask run`


## TODO
- Add color icon depends on time left
- Add testing

